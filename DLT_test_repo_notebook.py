# Databricks notebook source
import dlt
from pyspark.sql.functions import *
from pyspark.sql.types import *

TOPIC = "notifications-udp-notification-preferences-external-int"
KAFKA_BROKER = "b-1.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094, b-2.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094, b-3.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094,b-4.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094, b-5.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094, b-6.ent-msk-nonprod.pgcwe2.c11.kafka.us-east-1.amazonaws.com:9094"
CONSUMER_TRUSTSTORE_LOCATION = "/dbfs/FileStore/tmp/8be3b1be-99bf-424b-b378-d582e655708b"
CONSUMER_KEYSTORE_LOCATION = "/dbfs/FileStore/tmp/2dd16a4e-981f-4ab9-9602-720bfd1f6cb5"
CONSUMER_KEYSTORE_PASSWORD = "changeit"
CONSUMER_TRUSTSTORE_PASSWORD = "changeit"
KEY_PASSWORD = "changeit"
# subscribe to TOPIC at KAFKA_BROKER
df = (spark.read
    .format("kafka")
    .option("subscribe", TOPIC)
    .option("kafka.bootstrap.servers", KAFKA_BROKER)
    .option("kafka.security.protocol", "SSL")
    .option("kafka.ssl.key.password", KEY_PASSWORD)
    .option("kafka.bootstrap.servers", KAFKA_BROKER)   
    .option("kafka.ssl.truststore.location", CONSUMER_TRUSTSTORE_LOCATION)
    .option("kafka.ssl.keystore.location", CONSUMER_KEYSTORE_LOCATION)
    .option("kafka.ssl.keystore.password", CONSUMER_KEYSTORE_PASSWORD)
    .option("kafka.ssl.truststore.password", CONSUMER_TRUSTSTORE_PASSWORD)
    .option("kafka.group.id","udpr-frameworkint" )
    .load()
    )

raw_kafka_events=(df.selectExpr("cast(value as string) as value",
       "cast('notifications-udp-notification-preferences-external-int' as string) as topic",
       "current_timestamp() as timestamp",
       "cast(cast(value as string):alerttypecode[0] as decimal(10,0)) as alerttypecode",
       "cast(cast(value as string):createddatetime[0] as string) as createddatetime",
       "cast(cast(value as string):email[0] as string) as email",
       "cast(cast(value as string):label[0] as string) as label",
       "cast(cast(value as string):nmuniqueid[0] as string) as nmuniqueid",
       "cast(cast(value as string):push[0] as boolean) as push",
       "cast(cast(value as string):uniqueid[0] as string) as uniqueid",
       "cast(cast(value as string):updateddatetime[0] as string) as updateddatetime",
       "cast(cast(value as string):usertype[0] as string) as usertype",
       "cast(cast(value as string):web[0] as boolean) as web",
       "cast(cast(value as string):baseleid[0] as string) as baseleid"
))

# COMMAND ----------

# Use the function name as the table name
@dlt.create_table (table_properties={"pipelines.reset.allowed":"false"})
def raw_an_notification_preferences():
  return raw_kafka_events
